// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2018 Intel Corporation. All Rights Reserved.

#ifndef __RS_CONVERTER_CONVERTER_PNG_H
#define __RS_CONVERTER_CONVERTER_PNG_H

#include "../converter.hpp"


namespace converter {

    class converter_png : public converter_base {
        rs2_stream _streamType;
        std::string _filePath;
        rs2::colorizer _colorizer;

    public:
        converter_png(const std::string& filePath, rs2_stream streamType = rs2_stream::RS2_STREAM_ANY)
            : _filePath(filePath)
            , _streamType(streamType)
        {
        }

        std::string name() const override
        {
            return "PNG converter";
        }

        void convert(rs2::frameset& frameset) override
        {
			start_worker(
                [this, &frameset] {
							
                    for (size_t i = 0; i < frameset.size(); i++) {
                        rs2::video_frame frame = frameset[i].as<rs2::video_frame>();

                        if (frame && (_streamType == rs2_stream::RS2_STREAM_ANY || frame.get_profile().stream_type() == _streamType)) {
                            if (frames_map_get_and_set(frame.get_profile().stream_type(), frame.get_frame_number())) {
                                continue;
                            }

                            if (frame.get_profile().stream_type() == rs2_stream::RS2_STREAM_DEPTH)
                                frame = _colorizer.process(frame);
									
							std::stringstream filename;
                            filename << _filePath
                                << "_" << frame.get_profile().stream_name()
                                << "_" << frame.get_frame_number()
                                << ".png";

                            std::string filenameS = filename.str();

                            add_sub_worker(
                                [filenameS, frame] {
								cv::Mat imageBGR(cv::Size(frame.get_width(), frame.get_height()), CV_8UC3, (void*)frame.get_data(), cv::Mat::AUTO_STEP), imageRGB;
								cv::cvtColor(imageBGR, imageRGB, cv::COLOR_BGR2RGB);
								cv::imwrite(filenameS, imageRGB);
								//stbi_write_png(filenameS.c_str(), frame.get_width(), frame.get_height(), frame.get_bytes_per_pixel(), frame.get_data(), frame.get_stride_in_bytes());
                                });
                        }
                    }
                    wait_sub_workers();
                });
        }

		void convert(rs2::frameset& frameset, IOSet &io) override
		{
			start_worker(
				[this, &frameset, &io] {
				double currTimestamp = io.Timestamp;
				int currFrameID = io.FrameID;

				for (size_t i = 0; i < frameset.size(); i++) {
					rs2::video_frame frame = frameset[i].as<rs2::video_frame>();

					if (frame && (_streamType == rs2_stream::RS2_STREAM_ANY || frame.get_profile().stream_type() == _streamType)) {
						if (frames_map_get_and_set(frame.get_profile().stream_type(), frame.get_frame_number())) {
							continue;
						}

						if (frame.get_profile().stream_type() == rs2_stream::RS2_STREAM_DEPTH) {
							frame = _colorizer.process(frame);
						}

						currTimestamp = frame.get_timestamp();
						if (io.FrameID > 0)
							currFrameID += (int)round((currTimestamp - io.Timestamp) / (double)frame.get_profile().fps());
						else
							currFrameID++;
						io.Timestamp = currTimestamp;
						io.FrameID = currFrameID;

						char filenameC[256];
						sprintf_s(filenameC, "%s/FrameID%06d_TimeStamp%.0f.png", _filePath.c_str(), currFrameID, round(frame.get_timestamp()));
						std::string filenameS(filenameC);

						add_sub_worker(
							[filenameS, frame] {
							cv::Mat imageBGR(cv::Size(frame.get_width(), frame.get_height()), CV_8UC3, (void*)frame.get_data(), cv::Mat::AUTO_STEP), imageRGB;
							cv::cvtColor(imageBGR, imageRGB, cv::COLOR_BGR2RGB);
							cv::imwrite(filenameS, imageRGB);
							//stbi_write_png(filenameS.c_str(), frame.get_width(), frame.get_height(), frame.get_bytes_per_pixel(), frame.get_data(), frame.get_stride_in_bytes());
						});

					}
				}
				wait_sub_workers();
			});
		}
    };

}


#endif

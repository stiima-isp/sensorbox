// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2018 Intel Corporation. All Rights Reserved.

#ifndef __RS_CONVERTER_CONVERTER_PLY_H
#define __RS_CONVERTER_CONVERTER_PLY_H


#include "../converter.hpp"


namespace converter {

    class converter_ply : public converter_base {
    protected:
        std::string _filePath;

    public:
        converter_ply(const std::string& filePath)
            : _filePath(filePath)
        {
        }

        std::string name() const override
        {
            return "PLY converter";
        }

        void convert(rs2::frameset& frameset) override
        {
            rs2::pointcloud pc;
            start_worker(
                [this, &frameset, pc]() mutable {
                    auto frameDepth = frameset.get_depth_frame();
                    auto frameColor = frameset.get_color_frame();

                    if (frameDepth && frameColor) {
                        if (frames_map_get_and_set(rs2_stream::RS2_STREAM_ANY, frameDepth.get_frame_number())) {
                            return;
                        }

                        pc.map_to(frameColor);

                        auto points = pc.calculate(frameDepth);

                        std::stringstream filename;
                        filename << _filePath
                            << "_" << frameDepth.get_frame_number()
                            << ".ply";

                        points.export_to_ply(filename.str(), frameColor);
                    }
                });
        }

		void convert(rs2::frameset& frameset, IOSet &io) override
		{
			rs2::pointcloud pc;
			start_worker(
				[this, &frameset, pc, &io]() mutable {
				auto frameDepth = frameset.get_depth_frame();
				auto frameColor = frameset.get_color_frame();
				double currTimestamp = io.Timestamp;
				int currFrameID = io.FrameID;

				if (frameDepth && frameColor) {
					if (frames_map_get_and_set(rs2_stream::RS2_STREAM_ANY, frameDepth.get_frame_number())) {
						return;
					}

					pc.map_to(frameColor);

					auto points = pc.calculate(frameDepth);

					currTimestamp = frameDepth.get_timestamp();
					if (io.FrameID > 0)
						currFrameID += (int)round((currTimestamp - io.Timestamp) / (double)frameDepth.get_profile().fps());
					else
						currFrameID++;
					io.Timestamp = currTimestamp;
					io.FrameID = currFrameID;

					char filenameC[256];
					sprintf_s(filenameC, "%s/FrameID%06d_TimeStamp%.0f.ply", _filePath.c_str(), currFrameID, round(frameDepth.get_timestamp()));
					std::string filenameS(filenameC);

					points.export_to_ply(filenameS, frameColor);
				}
			});
		}
    };

}


#endif

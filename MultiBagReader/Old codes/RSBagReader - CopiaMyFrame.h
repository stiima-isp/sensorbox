#pragma once

#include <afxwin.h>
#include <iostream>
#include <vector>
#include <librealsense2\rs.hpp>
#include <fstream>

using namespace std;

#ifdef WIN32
#include <experimental/filesystem>
namespace fs = experimental::filesystem::v1;
#else
#include <experimental/filesystem>
namespace fs = experimental::filesystem;
#endif

#include "converters/converter-csv.hpp"
#include "converters/converter-png.hpp"
#include "converters/converter-ply.hpp"
#include "converters/converter-raw.hpp"


class CRSBagReader : public CWinThread
{
	DECLARE_DYNCREATE(CRSBagReader)

public:
	CRSBagReader();
	virtual ~CRSBagReader();

	virtual BOOL InitInstance();
	virtual BOOL ExitInstance();
	virtual BOOL Run();

	struct MyFrame {
		rs2::frame rsframe;
		size_t framenumber;
		double timestamp;
		rs2_stream streamType;
		int fps;
		int referenceCamera;
		int indexInFrameset;
	};

protected:
	DECLARE_MESSAGE_MAP()

public:	// List of functions
	chrono::nanoseconds LoadBagFile(rs2::pipeline & pipe, string filename, int refCamera);
	int ReadFrame(rs2::pipeline & pipe, size_t & framenumber, rs2::frameset & frameset);
	int ReadFrame(int readEveryNFrames, int idxFile, vector<rs2::pipeline>& pipes, vector<rs2::frameset>& framesets, vector<MyFrame>& frames);
	int ReadFrame(rs2::pipeline & pipe, size_t & framenumber, rs2::frameset & frameset, int readEveryNFrames);
	int ReadFrameAt(double targetTimestamp, int idxFile, vector<rs2::pipeline>& pipes, vector<rs2::frameset>& framesets, vector<MyFrame>& frames);
	int ReadFrameAt(double timestamp, rs2::pipeline & pipe, size_t & framenumber, rs2::frameset & frameset);
	int SelectBagFileNames();
	BOOL InitPipes();
	BOOL InitConverters();
	void CheckFrameRates();
	void EnableCheckOptions();
	int SetConverter(rs2::pipeline & pipe, int refCamera);
	const string currentDateTime();
	char * ComputePoseString(rs2::frameset & frameset);
	const wchar_t * CRSBagReader::GetWChar(const char *c);
	void SetContainer(void* Container);
	void AppendLogMessages(LPCTSTR Message);
	void EmptyLogMessages();
	void ResetDialog();

public:	// List of variables
	// I/O variables
	string inputPath, refCamFileName, BaseFileName;
	fs::path BaseDir;
	ofstream ofsPose;
	void *p_Container;

	// RS variables
	vector<rs2::pipeline> pipes;
	vector<size_t> frameNumbers;
	vector<rs2::frameset> framesets, framesetsAligned;
	vector<string> bagFileNames;
	int minFps;

	// Conversion variables
	int nFiles;
	int idxTrackingPipe;
	struct MyConverter {
		shared_ptr<converter::converter_base> converter;
		converter::converter_base::IOSet converterIO;
		bool bAlign;
		int referenceCamera;
	};
	vector<MyConverter> Converters;
	vector<MyFrame> Frames;
	bool bConversionLoop;

	// Save parameters
	int frameIncrement;
	bool bConvertPose;
	bool bConvertPngColor;
	bool bConvertPngDepth;
	bool bConvertPngDepthAligned;
	bool bConvertRawColor;
	bool bConvertRawDepth;
	bool bConvertRawDepthAligned;
	bool bConvertCsvDepthMeter;
	bool bConvertCsvDepthMeterAligned;
	bool bConvertPly;

};


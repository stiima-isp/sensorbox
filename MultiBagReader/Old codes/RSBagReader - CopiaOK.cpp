#include "pch.h"
#include "RSBagReader.h"
#include "MultiBagReaderDlg.h"
#include "resource.h"

IMPLEMENT_DYNCREATE(CRSBagReader, CWinThread)


CRSBagReader::CRSBagReader()
	: bConversionLoop(false)
	, frameIncrement(1)
	, bConvertPose(false)
	, bConvertPngColor(false)
	, bConvertPngDepth(false)
	, bConvertPngDepthAligned(false)
	, bConvertRawColor(false)
	, bConvertRawDepth(false)
	, bConvertRawDepthAligned(false)
	, bConvertCsvDepthMeter(false)
	, bConvertCsvDepthMeterAligned(false)
	, bConvertPly(false)
{
	m_bAutoDelete = false;
}


CRSBagReader::~CRSBagReader()
{
}

BOOL CRSBagReader::InitInstance()
{
	return TRUE;
}

int CRSBagReader::ExitInstance()
{
	if (ofsPose.is_open())
		ofsPose.close();
	m_hThread = NULL;
	return CWinThread::ExitInstance();
}


BEGIN_MESSAGE_MAP(CRSBagReader, CWinThread)
END_MESSAGE_MAP()

BOOL CRSBagReader::Run()
{
	auto tic = chrono::high_resolution_clock::now();

	// State if alignment is needed
	bool startAligning = false;
	for (int i = 0; i < Converters.size(); i++) {
		if (Converters[i].bAlign) {
			startAligning = true;
			break;
		}
	}

	// Read the first framesets of the input bagfiles
	for (int i = 0; i < nFiles; i++) {
		rs2::frameset frameset;
		ReadFrame(pipes[i], frameNumbers[i], frameset);
		framesets.push_back(frameset);
	}

	// Find the maximum timestamp of the first frames of the input bag files. It is the start time tStart
	double tStart = 0, t0;
	for (int i = 0; i < nFiles; i++) {
		t0 = framesets[i].get_timestamp();
		if (t0 > tStart)
			tStart = t0;
	}
	//tStart += 1000.0;	// Add 1 sec to be sure that all sensors were warm

	// Discard the initial frames with no correspondances before the tStart timestamp to synchronize the bag videos
	for (int i = 0; i < nFiles; i++) {
		int nRet = ReadFrameAt(tStart, pipes[i], frameNumbers[i], framesets[i]);
		double t = framesets[i].get_timestamp();
		if (nRet == EOF) {
			bConversionLoop = false;
			break;
		}
	}

	// Read the synchronized frames using the reference timestamp of the first camera, until the end of the file
	double PoseTimestamp = -1.0;
	int PoseFrameID = 0;
	int dispFrameID = 0;
	while (bConversionLoop)
	{
		int nRet;
		framesetsAligned.clear();
		
		// Display for single input bag
		if (nFiles == 1) {
			CString tString;
			tString.Format(L"Converting frame %d\r\n", Converters[0].converterIO.FrameID);
			AppendLogMessages(tString);
		}			

		// Read the other bags at the timestamp of the reference frameset (of the first camera)
		for (int i = 0; i < nFiles; i++) {
			
			if (i == 0) // Set the reference frameset
				nRet = ReadFrame(pipes[0], frameNumbers[0], framesets[0], frameIncrement);
			else // Read the other pipelines at the timestamp of the reference
				nRet = ReadFrameAt(framesets[0].get_timestamp(), pipes[i], frameNumbers[i], framesets[i]);
			
			if (nRet == EOF)
				bConversionLoop = false;
			else {
				if (startAligning) { // Create alignment if needed
					if (i == idxTrackingPipe)
						framesetsAligned.push_back(framesets[i]);
					else {
						rs2::align align_to_color(RS2_STREAM_COLOR);
						framesetsAligned.push_back(align_to_color.process(framesets[i]));
					}
				}
			}
		}

		if (!bConversionLoop)
			break;

		if (dispFrameID == Converters[0].converterIO.FrameID) {
			dispFrameID++;
			for (int i = 1; i < nFiles; i++) {
				if (i > 0) {
					// Show time lag between the synchronized frames (which should be < 1/fps). 
					CString tString;
					tString.Format(L"Frame ID: %d\tTime lag between Camera%d and Camera0 = %llu ms\r\n", dispFrameID, i, (size_t)(framesets[i].get_timestamp() - framesets[0].get_timestamp()));
					AppendLogMessages(tString);
				}
			}
		}		
		
		for (int i = 0; i < Converters.size(); i++) {
			if (Converters[i].bAlign)
				Converters[i].converter->convert(framesetsAligned[Converters[i].referenceCamera], Converters[i].converterIO);
			else
				Converters[i].converter->convert(framesets[Converters[i].referenceCamera], Converters[i].converterIO);
		}

		for (int i = 0; i < Converters.size(); i++) // Wait for thread answers (by joining them)
			Converters[i].converter->wait();

		if (ofsPose.is_open() && idxTrackingPipe >= 0) {

			double currTimestamp = framesets[idxTrackingPipe].get_timestamp();
			if (currTimestamp != PoseTimestamp) {
				if (PoseFrameID > 0)
					PoseFrameID = PoseFrameID + (int)round((currTimestamp - PoseTimestamp) / (double)minFps);
				else
					PoseFrameID++;
				PoseTimestamp = currTimestamp;
			
				char PoseCharArray[256];
				strcpy_s(PoseCharArray, 256, ComputePoseString(framesets[idxTrackingPipe]));
				ofsPose << PoseFrameID << "\t" << PoseCharArray << endl;
			}
		}		
	}

	auto toc = chrono::high_resolution_clock::now();
	auto timeElapsed = chrono::duration_cast<chrono::milliseconds>(toc - tic);
	
	CString tString;
	tString.Format(L"Conversion completed: %d frame(s) converted in %.3f seconds\r\n", Converters[0].converterIO.FrameID, timeElapsed.count()/1000.0);
	AppendLogMessages(tString);

	// Reset as Stop button was clicked 
	ResetDialog();

	// Exit thread
	ExitInstance();

	return TRUE;
}

// LoadBagFile: load a bag file to a pipeline
chrono::nanoseconds  CRSBagReader::LoadBagFile(rs2::pipeline & pipe, string filename, int refCamera)
{
	rs2::config cfg;
	cfg.enable_device_from_file(filename);
	pipe.start(cfg);

	auto device = pipe.get_active_profile().get_device();
	rs2::playback playback = device.as<rs2::playback>();
	playback.pause();
	playback.set_real_time(false);

	return playback.get_duration();
}

// ReadFrame: read the next frame of a bag-file-playback-pipeline
int CRSBagReader::ReadFrame(rs2::pipeline & pipe, size_t & framenumber, rs2::frameset & frameset)
{
	auto device = pipe.get_active_profile().get_device();
	rs2::playback playback = device.as<rs2::playback>();

	playback.resume();
	frameset = pipe.wait_for_frames();
	playback.pause();

	// If the new framenumber is lower than the previous (input), the playback is restarting (the bag was finished)
	if (frameset[0].get_frame_number() < framenumber)
		return EOF;

	framenumber = frameset[0].get_frame_number();

	return 0;
}


// ReadFrame: read the next frame of a bag-file-playback-pipeline
int CRSBagReader::ReadFrame(rs2::pipeline & pipe, size_t & framenumber, rs2::frameset & frameset, int readEveryNFrames)
{
	auto device = pipe.get_active_profile().get_device();
	rs2::playback playback = device.as<rs2::playback>();

	bool isEOF = false;
	size_t prevFramenumber = framenumber;

	playback.resume();
	while (true) {
		frameset = pipe.wait_for_frames();
		for (int i = 0; i < frameset.size(); i++)
			TRACE("%s %d\n", frameset[i].get_profile().stream_name().c_str(), frameset[i].get_frame_number());
		framenumber = frameset[0].get_frame_number();
		if (framenumber == prevFramenumber + readEveryNFrames) {
			isEOF = false;
			break;
		}
		else if (framenumber < prevFramenumber) {	// If the new framenumber is lower than the previous (input), the playback is restarting (the bag was finished)
			isEOF = true;
			break;
		}
	}
	playback.pause();

	if (isEOF)
		return EOF;

	return 0;
}

// ReadFrameAt: read the frame just after the specified timestamp
int CRSBagReader::ReadFrameAt(double timestamp, rs2::pipeline & pipe, size_t & framenumber, rs2::frameset & frameset)
{
	/*
	// Keep reading the frames till the new timestamp of the frameset is higher than the input timestamp or till the file ends
	while (true) {
		if (frameset.get_timestamp() >= timestamp)
			return 0;

		int r = ReadFrame(pipe, framenumber, frameset, 1);
		if (r == EOF)
			return EOF;
	}
	return 0;	
	*/
	int nRet = 0;
	auto device = pipe.get_active_profile().get_device();
	rs2::playback playback = device.as<rs2::playback>();

	playback.resume();
	
	// Keep reading the frames till the new timestamp of the frameset is higher than the input timestamp or till the file ends
	while (frameset.get_timestamp() < timestamp && nRet != EOF) {
		
		frameset = pipe.wait_for_frames();
		if (frameset[0].get_frame_number() < framenumber) {
			nRet = EOF;
			break;
		}
		framenumber = frameset[0].get_frame_number();

	}
	playback.pause();
	if (frameset.get_timestamp() >= timestamp)
		return 0;
	else
		return EOF;
}

// SelectBagFileNames: process the input filename, create the basename and a vector of names of bags
int CRSBagReader::SelectBagFileNames()
{
	EmptyLogMessages();
	BaseFileName.clear();

	if (bagFileNames.size() > 0)
		bagFileNames.clear();

	if (!strstr(refCamFileName.c_str(), ".bag")) {
		refCamFileName = refCamFileName + ".bag";
		
		CString tString;
		tString.Format(L"Missing \".bag\" extension to the name of the base file. Considering %s...\r\n", GetWChar(refCamFileName.c_str()));
		AppendLogMessages(tString);
	}
		
	if (!strstr(refCamFileName.c_str(), "_IntelRealSense") && !strstr(refCamFileName.c_str(), "_SN")) {
		// Generic input files. Only 1 camera expected!
		BaseFileName = refCamFileName.substr(0, refCamFileName.length() - 4);
		bagFileNames.push_back(inputPath + refCamFileName);

		CString tString;
		tString.Format(L"Generic input bag\r\nLoading \"%s\"\r\n", GetWChar(refCamFileName.c_str()));
		AppendLogMessages(tString);
	}
	else {
		// Bag files are from SensorBox
		BaseFileName = refCamFileName.substr(0, refCamFileName.length() - 38);
		bagFileNames.push_back(inputPath + refCamFileName);

		CString tString;
		tString.Format(L"Input bag(s) from the SensorBox\r\nLoading \"%s\" as Camera0 (reference)\r\n", GetWChar(refCamFileName.c_str()));
		AppendLogMessages(tString);

		int indCam = 1;
		for (auto& p : fs::directory_iterator(inputPath))
		{
			string tempFileName = p.path().filename().generic_string();
			if (tempFileName.find(BaseFileName) == 0 && tempFileName.find("_IntelRealSense") == BaseFileName.length()
				&& tempFileName.find("_SN") == BaseFileName.length() + 19 && tempFileName.find(".bag") != string::npos &&
				tempFileName.compare(refCamFileName) != 0 && tempFileName.find("_SN") == tempFileName.length() - 19)
			{
				tString.Format(L"Loading \"%s\" as Camera%d\r\n", GetWChar(tempFileName.c_str()), indCam);
				AppendLogMessages(tString);
				bagFileNames.push_back(inputPath + tempFileName);
				indCam++;
			}
		}
	}

	int nRet = bagFileNames.size();
	return nRet;
}


BOOL CRSBagReader::InitPipes()	// TODO: Add control on pipe exceptions
{
	frameNumbers.clear();
	framesets.clear();
	pipes.clear();
	// Load bag files 
	for (int i = 0; i < nFiles; i++) {
		rs2::pipeline pipe;
		LoadBagFile(pipe, bagFileNames[i], i);
		pipes.push_back(pipe);
		frameNumbers.push_back(0ULL);
	}

	CheckFrameRates();		
	return FALSE;
}

BOOL CRSBagReader::InitConverters()
{
	// Init Output Directory
	BaseDir = inputPath + BaseFileName + currentDateTime();
	if (!fs::create_directories(BaseDir)) {
		AfxMessageBox(L"Cannot Create Output Directory");
		return TRUE;
	}
	CString tString;
	tString.Format(L"Destination folder: \"%s\"\r\n", GetWChar(BaseDir.generic_string().c_str()));
	AppendLogMessages(tString);

	// Inizializations
	Converters.clear();
	idxTrackingPipe = -1;
	for (int i = 0; i < nFiles; i++) {
		int nRet = SetConverter(pipes[i], i);
		if (nRet == 1)
			idxTrackingPipe = i;
	}

	// Check if converters are created
	if (Converters.size() == 0 && !ofsPose.is_open()) {
		AfxMessageBox(L"Empty converters");
		return TRUE;
	}
	   
	return FALSE;
}

void CRSBagReader::CheckFrameRates()
{
	minFps = INT_MAX;
	int iMinFps = nFiles;

	for (int i = 0; i < nFiles; i++) {
		rs2::pipeline pipe;
		pipe = pipes[i];
		vector<rs2::stream_profile> streams = pipe.get_active_profile().get_streams();
		int tMinFps = 1000;
		for (int s = 0; s < streams.size(); s++)
			if (streams[s].fps() < tMinFps)
				tMinFps = streams[s].fps();
		if (tMinFps < minFps) {
			minFps = tMinFps;
			iMinFps = i;
		}
	}

	if (iMinFps != 0) {
		string name0 = bagFileNames[0].substr(inputPath.length(), bagFileNames[0].length() - inputPath.length());
		string nameMin = bagFileNames[iMinFps].substr(inputPath.length(), bagFileNames[iMinFps].length() - inputPath.length());
		CString tString;
		tString.Format(L"The framerate in bag \"%s\" is higher than the one in bag \"%s\"\r\nChanging the reference camera accordingly...\r\n", GetWChar(name0.c_str()), GetWChar(nameMin.c_str()));
		AppendLogMessages(tString);
	}

	pipes.clear();
	for (int i = 0; i < nFiles; i++) {
		rs2::pipeline pipe;
		LoadBagFile(pipe, bagFileNames[(iMinFps+i)%nFiles], (iMinFps + i) % nFiles);
		pipes.push_back(pipe);
	}
}


void CRSBagReader::EnableCheckOptions()
{
	CMultiBagReaderDlg *pDlg = (CMultiBagReaderDlg *)p_Container;

	for (int i = 0; i < nFiles; i++) {
		rs2::pipeline pipe;
		pipe = pipes[i];

		auto device = pipe.get_active_profile().get_device();
		string serial = device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
		string name = device.get_info(RS2_CAMERA_INFO_NAME);

		CString tString;
		if (i == 0)
			tString.Format(L"Camera%d (reference): %s (SN %s)\r\n", i, GetWChar(name.c_str()), GetWChar(serial.c_str()));
		else 
			tString.Format(L"Camera%d: %s (SN %s)\r\n", i, GetWChar(name.c_str()), GetWChar(serial.c_str()));
		AppendLogMessages(tString);

		vector<rs2::stream_profile> streams = pipe.get_active_profile().get_streams();
		bool hasColor = false;
		bool hasDepth = false;

		for (int s = 0; s < streams.size(); s++) {
			if (streams[s].stream_type() == RS2_STREAM_POSE) {
				bConvertPose = true;
				pDlg->m_checkPose.SetCheck(BST_CHECKED);
				pDlg->m_checkPose.EnableWindow(TRUE);
			}
			else if (streams[s].stream_type() == RS2_STREAM_COLOR) {
				hasColor = true;
				bConvertPngColor = true;
				pDlg->m_checkPngColor.SetCheck(BST_CHECKED);
				pDlg->m_checkPngColor.EnableWindow(TRUE);
				bConvertRawColor = false;
				pDlg->m_checkRawColor.SetCheck(BST_UNCHECKED);
				pDlg->m_checkRawColor.EnableWindow(TRUE);
			}
			else if (streams[s].stream_type() == RS2_STREAM_DEPTH) {
				hasDepth = true;
				bConvertPngDepth = true;
				pDlg->m_checkPngDepth.SetCheck(BST_CHECKED);
				pDlg->m_checkPngDepth.EnableWindow(TRUE);
				bConvertPngDepthAligned = false;
				pDlg->m_checkPngDepthAligned.SetCheck(BST_UNCHECKED);
				pDlg->m_checkPngDepthAligned.EnableWindow(TRUE);
				bConvertRawDepth = false;
				pDlg->m_checkRawDepth.SetCheck(BST_UNCHECKED);
				pDlg->m_checkRawDepth.EnableWindow(TRUE);
				bConvertRawDepthAligned = false;
				pDlg->m_checkRawDepthAligned.SetCheck(BST_UNCHECKED);
				pDlg->m_checkRawDepthAligned.EnableWindow(TRUE);
				bConvertCsvDepthMeter = false;
				pDlg->m_checkCsvDepthMeter.SetCheck(BST_UNCHECKED);
				pDlg->m_checkCsvDepthMeter.EnableWindow(TRUE);
				bConvertCsvDepthMeterAligned = false;
				pDlg->m_checkCsvDepthMeterAligned.SetCheck(BST_UNCHECKED);
				pDlg->m_checkCsvDepthMeterAligned.EnableWindow(TRUE);
			}
		}
		if (hasColor && hasDepth) {
			bConvertPly = false;
			pDlg->m_checkPly.SetCheck(BST_UNCHECKED);
			pDlg->m_checkPly.EnableWindow(TRUE);
		}				
	}
}



const wchar_t * CRSBagReader::GetWChar(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* outWChar = new wchar_t[cSize];
	size_t outSize;
	mbstowcs_s(&outSize, outWChar, cSize, c, cSize - 1);

	return outWChar;
}

const string CRSBagReader::currentDateTime()
{
	time_t     now = time(0);
	struct tm  tstruct;
	localtime_s(&tstruct, &now);
	char       buf[80];
	strftime(buf, sizeof(buf), "_%Y%m%d_%H%M%S", &tstruct);
	return buf;
}

char * CRSBagReader::ComputePoseString(rs2::frameset & frameset)
{
	// Get a frame from the pose stream
	auto f = frameset.first_or_default(RS2_STREAM_POSE);
	// Cast the frame to pose_frame and get its data
	rs2_pose pose_data = f.as<rs2::pose_frame>().get_pose_data();

	rs2_vector t = pose_data.translation;
	rs2_quaternion q = pose_data.rotation;

	char PoseStr[256];
	sprintf_s(PoseStr, 256, "%.0f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f", frameset.get_timestamp(), t.x, t.y, t.z, q.w, q.x, q.y, q.z);

	return PoseStr;
}

int CRSBagReader::SetConverter(rs2::pipeline & pipe, int refCamera)
{
	auto device = pipe.get_active_profile().get_device();
	string serial = device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
	string name = device.get_info(RS2_CAMERA_INFO_NAME);

	if (name.find("T265") == string::npos) {

		if (bConvertPngColor) {
			fs::path subDirectory = BaseDir.generic_string() + "/Color/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_png>(subDirectory.generic_string(), RS2_STREAM_COLOR);
			tConverter.bAlign = false;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertPngDepth) {
			fs::path subDirectory = BaseDir.generic_string() + "/Depth/" + serial;
			fs::create_directories(subDirectory);
			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_png>(subDirectory.generic_string(), RS2_STREAM_DEPTH);
			tConverter.bAlign = false;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertPngDepthAligned) {
			fs::path subDirectory = BaseDir.generic_string() + "/DepthAligned/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_png>(subDirectory.generic_string(), RS2_STREAM_DEPTH);
			tConverter.bAlign = true;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertRawColor) {
			fs::path subDirectory = BaseDir.generic_string() + "/ColorRaw/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_raw>(subDirectory.generic_string(), RS2_STREAM_COLOR);
			tConverter.bAlign = false;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertRawDepth) {
			fs::path subDirectory = BaseDir.generic_string() + "/DepthRaw/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_raw>(subDirectory.generic_string(), RS2_STREAM_DEPTH);
			tConverter.bAlign = false;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertRawDepthAligned) {
			fs::path subDirectory = BaseDir.generic_string() + "/DepthRawAligned/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_raw>(subDirectory.generic_string(), RS2_STREAM_DEPTH);
			tConverter.bAlign = true;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertCsvDepthMeter) {
			fs::path subDirectory = BaseDir.generic_string() + "/DepthMeter/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_csv>(subDirectory.generic_string(), RS2_STREAM_ANY);
			tConverter.bAlign = false;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertCsvDepthMeterAligned) {
			fs::path subDirectory = BaseDir.generic_string() + "/DepthMeterAligned/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_csv>(subDirectory.generic_string(), RS2_STREAM_ANY);
			tConverter.bAlign = true;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (bConvertPly) {
			fs::path subDirectory = BaseDir.generic_string() + "/PointCloud/" + serial;
			fs::create_directories(subDirectory);

			MyConverter tConverter;
			tConverter.converter = make_shared<converter::converter_ply>(subDirectory.generic_string());
			tConverter.bAlign = false;
			tConverter.referenceCamera = refCamera;
			tConverter.converterIO.FrameID = 0;
			tConverter.converterIO.Timestamp = -1.0;
			Converters.push_back(tConverter);
		}

		if (Converters.empty())
			return -1;
	}
	else {
		if (bConvertPose) {
			string PoseFileName = BaseDir.generic_string() + "/Pose.txt";
			ofsPose.open(PoseFileName.c_str(), std::ofstream::out | std::ofstream::app);
			char firstLine[64];
			sprintf_s(firstLine, 64, "FrameID\tTimeStamp [ms]\tTx\tTy\tTz\tRw\tRx\tRy\tRz");
			ofsPose << firstLine << endl;
		}
		return 1;
	}

	return 0;
}

void CRSBagReader::SetContainer(void* Container)
{
	p_Container = Container;
}

void CRSBagReader::AppendLogMessages(LPCTSTR Message)
{
	CMultiBagReaderDlg *pDlg = (CMultiBagReaderDlg *)p_Container;
	
	// get the initial text length
	int nLength = pDlg->m_editLog.GetWindowTextLength();
	// put the selection at the end of text
	pDlg->m_editLog.SetSel(nLength, nLength);
	// replace the selection
	pDlg->m_editLog.ReplaceSel(Message);
}

void CRSBagReader::EmptyLogMessages()
{
	CMultiBagReaderDlg *pDlg = (CMultiBagReaderDlg *)p_Container;
	pDlg->m_editLog.SetWindowText(L"");
}

void CRSBagReader::ResetDialog()
{
	CMultiBagReaderDlg *pDlg = (CMultiBagReaderDlg *)p_Container;

	pDlg->GetDlgItem(IDC_STOP)->EnableWindow(FALSE);
	pDlg->GetDlgItem(IDC_START)->EnableWindow(FALSE);
	pDlg->GetDlgItem(IDC_SETREFERENCE)->EnableWindow(TRUE);
	inputPath = "";
	refCamFileName = "";
	pDlg->InitializeCheckControls();
}



// ConsoleAligner.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

/*
struct synthframe {
	int W, H, B;
	std::vector<uint8_t> cdat;
	std::vector<uint16_t> ddat;
};

synthframe cframe; //Color-Frame
synthframe dframe; //Depth-Frame

// init windows
cv::String m_winColorName = "RGB Image";
cv::String m_winDepthName = "Depth Image";

synthframe& getColor() {
	
	cv::Mat color_image = cv::imread("C:\\Users\\rober\\Downloads\\prova\\SN135122079569_ID008_Color_1635416123164.png", cv::IMREAD_COLOR);

	if (color_image.isContinuous()) {
		// cframe.dat.assign(color_image.datastart, color_image.dataend); // <- has problems for sub-matrix like mat = big_mat.row(i)
		cframe.cdat.assign(color_image.data, color_image.data + color_image.total() * color_image.channels());
	}
	else {
		for (int i = 0; i < color_image.rows; ++i) {
			cframe.cdat.insert(cframe.cdat.end(), color_image.ptr<uchar>(i), color_image.ptr<uchar>(i) + color_image.cols * color_image.channels());
		}
	}

	cframe.W = CW;
	cframe.H = CH;
	cframe.B = CB;

	return cframe;
}


synthframe& getDepth() {

	cv::Mat depth_image;
	FILE* fp = NULL;
	uint16_t* imagedata = NULL;
	int framesize = DW * DH;

	//Open raw Bayer image.
	fopen_s(&fp, "C:\\Users\\rober\\Downloads\\prova\\SN135122079569_ID008_Depth_1635416123176.raw", "rb");

	if (fp) {
		//Memory allocation for bayer image data buffer.
		imagedata = (uint16_t*)malloc(sizeof(uint16_t) * framesize);

		//Read image data and store in buffer.
		fread_s(imagedata, sizeof(uint16_t) * framesize, DB, framesize, fp);

		//Create Opencv mat structure for image dimension. For 8 bit bayer, type should be CV_8UC1.
		depth_image.create(DH, DW, CV_16UC1);

		memcpy_s(depth_image.data, DW*DH*DB, imagedata, framesize*sizeof(uint16_t));
		free(imagedata);
		fclose(fp);
	}	

	//cv::Mat depth_image = cv::imread("C:\\Users\\rober\\Downloads\\prova\\SN135122079569_ID008_Depth_1635416123176.raw", cv::IMREAD_UNCHANGED);
	if (depth_image.isContinuous()) {
		// cframe.dat.assign(depth_image.datastart, depth_image.dataend); // <- has problems for sub-matrix like mat = big_mat.row(i)
		dframe.ddat.assign(depth_image.data, depth_image.data + depth_image.total() * depth_image.channels());
	}
	else {
		for (int i = 0; i < depth_image.rows; ++i) {
			dframe.ddat.insert(dframe.ddat.end(), depth_image.ptr<uchar>(i), depth_image.ptr<uchar>(i) + depth_image.cols * depth_image.channels());
		}
	}

	dframe.W = DW;
	dframe.H = DH;
	dframe.B = DB;
	return dframe;
}

*/




#include <iostream>
#include <opencv2/opencv.hpp>
#include <librealsense2/rs.hpp>
#include <librealsense2/hpp/rs_internal.hpp>


const int CW = 1280; //Color Frame
const int CH = 720;
const int CB = 3;

const int DW = 1280; //Depth Frame
const int DH = 720;
const int DB = 2;


int main() {

	rs2::software_device dev;

	rs2::software_sensor depth_sensor = dev.add_sensor("Depth");
	rs2::software_sensor color_sensor = dev.add_sensor("Color");

	const rs2_intrinsics color_intrinsics{ CW, CH, 643.827, 346.182, 903.946, 901.97, 
		RS2_DISTORTION_INVERSE_BROWN_CONRADY, { 0,0,0,0,0 } };

	const rs2_intrinsics depth_intrinsics{ DW, DH, 645.027, 356.917, 634.816, 634.816,
		RS2_DISTORTION_INVERSE_BROWN_CONRADY, { 0,0,0,0,0 } };

	rs2_video_stream depth_stream({ RS2_STREAM_DEPTH, 0, 0, DW, DH, 100, DB, RS2_FORMAT_Z16, depth_intrinsics });
	rs2_video_stream color_stream({ RS2_STREAM_COLOR, 0, 1, CW, CH, 100, CB, RS2_FORMAT_BGRA8, color_intrinsics });
	
	depth_sensor.add_read_only_option(RS2_OPTION_DEPTH_UNITS, 0.001f);
	depth_sensor.add_read_only_option(RS2_OPTION_STEREO_BASELINE, 0.001f);
	
	auto depth_stream_profile = depth_sensor.add_video_stream(depth_stream);
	auto color_stream_profile = color_sensor.add_video_stream(color_stream);

	/*color_stream_profile.register_extrinsics_to(depth_stream_profile, {
		{ 0.999793,-0.0203345,0.00102088,
		0.0203363,0.999792,-0.00176653,
		-0.000984747,0.00178692,0.999998 },
		{-0.0147762,-0.000381684,0.0000566639} });

	*/

	dev.create_matcher(RS2_MATCHER_DEFAULT);
	rs2::syncer sync;

	depth_sensor.open(depth_stream_profile);
	color_sensor.open(color_stream_profile);

	depth_sensor.start(sync);
	color_sensor.start(sync);

	/*depth_stream_profile.register_extrinsics_to(color_stream_profile, {
		{ 0.999793,0.0203363,-0.000984747,
		-0.0203345,0.999792,0.00178692,
		0.00102088,-0.00176653,0.999998 },
		{0.014781,0.0000810355,-0.0000422532} });*/

	depth_stream_profile.register_extrinsics_to(color_stream_profile, { {1, 0, 0, 0, 1, 0, 0, 0, 1},
		{ 0, 0, 0 }});
	color_stream_profile.register_extrinsics_to(depth_stream_profile, { {1, 0, 0, 0, 1, 0, 0, 0, 1},
		{ 0, 0, 0 } });
	
	rs2::align align_to_depth(RS2_STREAM_DEPTH);
	rs2::align align_to_color(RS2_STREAM_COLOR);

	rs2::frameset fs;
	cv::Mat color_image;
	cv::Mat depth_image;

	for (int idx = 0; idx < 2; idx++) {

		color_image = cv::imread("C:\\Users\\rober\\Downloads\\prova\\SN135122079569_ID008_Color_1635416123164.png", cv::IMREAD_COLOR);
	
		FILE* fp = NULL;
		uint16_t* imagedata = NULL;
		int framesize = DW * DH;
		//Open raw image
		fopen_s(&fp, "C:\\Users\\rober\\Downloads\\prova\\SN135122079569_ID008_Depth_1635416123176.raw", "rb");
		if (fp) {
			//Memory allocation for bayer image data buffer
			imagedata = (uint16_t*)malloc(sizeof(uint16_t) * framesize);
			//Read image data and store in buffer.
			fread_s(imagedata, sizeof(uint16_t) * framesize, DB, framesize, fp);
			//Create Opencv mat structure for image dimension. For 8 bit bayer, type should be CV_8UC1.
			depth_image.create(DH, DW, CV_16UC1);
			memcpy_s(depth_image.data, DW * DH * DB, imagedata, framesize * sizeof(uint16_t));
			free(imagedata);
			fclose(fp);
		}

		color_sensor.on_video_frame({ (void*)color_image.data, // Frame pixels from capture API
										 [](void*) {}, // Custom deleter (if required)
										 CB * CW, CB, // Stride and Bytes-per-pixel
										 (double)(idx) *10, RS2_TIMESTAMP_DOMAIN_HARDWARE_CLOCK,
										 idx, // Timestamp, Frame# for potential sync services
										 color_stream_profile });
		depth_sensor.on_video_frame({ (void*)depth_image.data, // Frame pixels from capture API
										 [](void*) {}, // Custom deleter (if required)
										 DB * DW, DB, // Stride and Bytes-per-pixel
										 (double)(idx) *10, RS2_TIMESTAMP_DOMAIN_HARDWARE_CLOCK,
										 idx, // Timestamp, Frame# for potential sync services
										 depth_stream_profile });

		fs = sync.wait_for_frames();
		/*if (fs.size() == 2) {
			rs2::frameset fsA2D = align_to_depth.process(fs);
			rs2::frameset fsA2C = align_to_color.process(fs);
			
			rs2::video_frame depth_frame = fsA2C.first_or_default(RS2_STREAM_DEPTH);
			rs2::video_frame color_frame = fsA2C.first_or_default(RS2_STREAM_COLOR);

			//cv::Mat aligned_image(color_frame.get_height(), color_frame.get_width(), CV_8UC3, (void*)(color_frame.get_data()), cv::Mat::AUTO_STEP);
			
			rs2::colorizer col;
			depth_frame = col.process(depth_frame);
			cv::Mat aligned_image(depth_frame.get_height(), depth_frame.get_width(), CV_8UC3, (void*)(depth_frame.get_data()), cv::Mat::AUTO_STEP);
			cv::cvtColor(aligned_image, aligned_image, cv::COLOR_BGR2RGB);
			
			cv::imwrite("C:\\Users\\rober\\Downloads\\prova\\aligned" + std::to_string(idx) + ".png", aligned_image);

			/*rs2::depth_frame depth_frame = fs.get_depth_frame();
			rs2::video_frame color_frame = fs.get_color_frame();

			rs2::pointcloud pc;

			if (depth_frame && color_frame) {
				pc.map_to(color_frame);
				auto points = pc.calculate(depth_frame);
				points.export_to_ply("C:\\Users\\rober\\Downloads\\prova\\pc.ply", color_frame);
			}
		}*/
	}
	

	if (fs.size() == 2) {
		rs2::frameset fsA2D = align_to_depth.process(fs);
		rs2::frameset fsA2C = align_to_color.process(fs);

		rs2::video_frame depth_frame = fsA2C.first_or_default(RS2_STREAM_DEPTH);
		rs2::video_frame color_frame = fsA2C.first_or_default(RS2_STREAM_COLOR);

		//cv::Mat aligned_image(color_frame.get_height(), color_frame.get_width(), CV_8UC3, (void*)(color_frame.get_data()), cv::Mat::AUTO_STEP);

		rs2::colorizer col;
		depth_frame = col.process(depth_frame);
		cv::Mat aligned_image(depth_frame.get_height(), depth_frame.get_width(), CV_8UC3, (void*)(depth_frame.get_data()), cv::Mat::AUTO_STEP);
		cv::cvtColor(aligned_image, aligned_image, cv::COLOR_BGR2RGB);

		cv::imwrite("C:\\Users\\rober\\Downloads\\prova\\aligned.png", aligned_image);
	}

	return 0;
}
